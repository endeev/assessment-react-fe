import React, { Component } from "react";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img
            src="http://www.endeev.com/wp-content/themes/incubator/images/endeev-colorlogo.png"
            alt="logo"
            width="350"
          />
          <h2>Endeev Frontend Exercise</h2>
          <a
            className="App-link"
            href="http://endeev.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Endeev.com
          </a>
        </header>
      </div>
    );
  }
}

export default App;
